group 'tech.ezgames'

//load config
apply from: 'gradle/configuration.gradle'

//set version
apply from: 'gradle/version.gradle'
println "Project version is $version"


buildscript {
    repositories {
        jcenter()
        maven {
            url 'https://plugins.gradle.org/m2/'
        }
    }
    dependencies {
        classpath 'org.springframework.boot:spring-boot-gradle-plugin:1.3.6.RELEASE'
        classpath 'com.netflix.nebula:nebula-project-plugin:3.1.0'
        classpath 'jp.classmethod.aws:gradle-aws-plugin:0.29'
    }
}

ext.artifactId = project.name

apply plugin: 'java'
apply plugin: 'groovy'
apply plugin: 'idea'
apply plugin: 'spring-boot'
apply plugin: 'war'
apply plugin: 'nebula.project'
apply plugin: 'nebula.facet'
apply plugin: 'jp.classmethod.aws.beanstalk'

//add contacts information
apply from: 'gradle/contacts.gradle'

facets {
    //each block represents another code module
    integTest {
        parentSourceSet = 'test'
        testTaskName = 'integrationTest'
        includeInCheckLifecycle = true
    }

//    simTest {
//        parentSourceSet = 'test' //inherit dependencies from test module
//        testTaskName = 'simulationTest'
//        includeInCheckLifecycle = false //should only run when run explicitly
//    }
//
//    funcTest {
//        parentSourceSet = 'test'
//        testTaskName = 'functionalTest'
//        includeInCheckLifecycle = true
//    }
}

repositories {
    jcenter()
}

sourceCompatibility = 1.8
targetCompatibility = 1.8


dependencies {
    //GROOVY!
    compile         'org.codehaus.groovy:groovy-all:2.4.6:indy'
    //SPRING libs
    compile         'org.springframework.boot:spring-boot-starter-web'
    //AWS libs
    compile         'com.amazonaws:aws-java-sdk-sqs'

    //PSQL
    compile 'org.postgresql:postgresql:9.4.1209'


    //mark embedded tomcat as provided so it won't be packaged in the war
    providedRuntime 'org.springframework.boot:spring-boot-starter-tomcat'

    testCompile     'junit:junit'
    testCompile     'org.springframework:spring-test'
    // Need to manually exclude groovy-all from being transitively brought in by spock
    // see http://blog.freeside.co/2014/06/24/gradle-and-groovys-invoke-dynamic-support/
    testCompile     ('org.spockframework:spock-core:1.0-groovy-2.4') {
        exclude module: 'groovy-all'
    }
    testCompile     ('org.spockframework:spock-spring:1.0-groovy-2.4') {
        exclude module: 'groovy-all'
    }
    testCompile     'org.hamcrest:hamcrest-all'
    testRuntime     'cglib:cglib:3.2.2'

//    testRuntime "org.objenesis:objenesis:2.1"    // allows mocking of classes without default constructor (together with CGLIB)

    // h2: in mem db good for testing
    testRuntime "com.h2database:h2:1.4.182"

}

//easy way to pass arguments to spring boot
bootRun {
    if (project.hasProperty('args')) {
        def bootArgs = project.args.split('\\s+')
        println "passing on to sprint boot arguments: $bootArgs"
        args bootArgs
    }
}

// Necessary once we added Groovy code to production
sourceSets.main.java.srcDirs = []
sourceSets.main.groovy.srcDirs += ["src/main/java"]


//compile groovy with indy support (invoke dynamic - works faster)
tasks.withType(GroovyCompile) {
    groovyOptions.optimizationOptions.indy = true
}

tasks.withType(Test) {
    testLogging {
        events 'started', 'passed'
    }
}

dependencyManagement {
    imports {
        mavenBom 'com.amazonaws:aws-java-sdk-bom:1.10.66'
    }
}

import org.apache.tools.ant.filters.*


processResources {
    filter ReplaceTokens, tokens : [
            "application.version" : String.valueOf(project.version)
    ]
}

aws {
    region = 'us-west-2'
}

beanstalk {
    appName 'slots-auditer'
    appDesc 'Save audit records to db'

    version {
        label = "$artifactId-$project.version"
        description = "$artifactId v$project.version"
        bucket = "elasticbeanstalk-${aws.region}-${aws.accountId}"
        key = "builds/$artifactId/${war.archiveName}"
    }

}

task uploadWarToS3(type: jp.classmethod.aws.gradle.s3.AmazonS3FileUploadTask, dependsOn: war) {
    group "AWS"
    description "Upload ${artifactId} application version to S3."

    bucketName "elasticbeanstalk-${aws.region}-${aws.accountId}"
    key "builds/$artifactId/${war.archiveName}"
    file war.archivePath

    overwrite project.version.toString().endsWith("-SNAPSHOT")
}

//skip the built in task to upload bundle - use our own to upload the war
awsEbUploadBundle.enabled = false

awsEbCreateApplicationVersion.dependsOn uploadWarToS3
