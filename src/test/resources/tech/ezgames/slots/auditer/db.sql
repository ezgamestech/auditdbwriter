CREATE TABLE accounts (
        accountid varchar(255) NOT NULL,
        deviceid varchar(255),
        appsflyerid varchar(255),
        deltadnaid varchar(255),
        facebookid varchar(255),
        ip varchar(255),
        time timestamp
);


CREATE TABLE purchases (
        time timestamp NOT NULL,
        accountid varchar(255) NOT NULL,
        deviceid varchar(255),
        store varchar(255) NOT NULL,
        productid varchar(255),
        transactionid varchar(255) NOT NULL,
        "cost" numeric(13,4) NOT NULL,
        currency char(3) NOT NULL,
        coins int8 NOT NULL,
        contextsrc varchar(255),
        contextid varchar(255)
);


CREATE TABLE spins (
        requestid varchar(255) NOT NULL,
        sessionid varchar(255) NOT NULL,
        time timestamp NOT NULL,
        accountid varchar(255) NOT NULL,
        deviceid varchar(255) NOT NULL,
        balance int8 NOT NULL,
        "level" int4,
        xp int8,
        gameid varchar(255) NOT NULL,
        reel varchar(255) NOT NULL,
        totalbet int8 NOT NULL,
        numlines int4 NOT NULL,
        isfreespin bool NOT NULL,
        bonus bool NOT NULL,
        bigwin bool NOT NULL,
        megawin bool NOT NULL,
        win int8
);

ALTER TABLE spins ADD COLUMN clientip varchar(255);
ALTER TABLE spins ADD COLUMN clientver varchar(255);
ALTER TABLE spins ADD COLUMN serverver varchar(255);

ALTER TABLE spins ADD COLUMN autospin bool;
ALTER TABLE spins ADD COLUMN maxbet bool;
ALTER TABLE spins ADD COLUMN force varchar(255);

ALTER TABLE spins ADD COLUMN totalXp BIGINT;


CREATE TABLE afevents (
        event_type varchar(255),
        attribution_type varchar(255),
        click_time timestamp,
        download_time timestamp,
        install_time timestamp,
        media_source varchar(255),
        agency varchar(255),
        af_channel varchar(255),
        af_keywords varchar(255),
        campaign varchar(255),
        af_c_id varchar(255),
        af_adset varchar(255),
        af_adset_id varchar(255),
        af_ad varchar(255),
        af_ad_id varchar(255),
        fb_campaign_name varchar(255),
        fb_campaign_id varchar(255),
        fb_adset_name varchar(255),
        fb_adset_id varchar(255),
        fb_adgroup_name varchar(255),
        fb_adgroup_id varchar(255),
        af_ad_type varchar(255),
        af_siteid varchar(255),
        af_sub1 varchar(255),
        af_sub2 varchar(255),
        af_sub3 varchar(255),
        af_sub4 varchar(255),
        af_sub5 varchar(255),
        http_referrer varchar(255),
        click_url varchar(255),
        af_cost_model varchar(255),
        af_cost_value varchar(255),
        af_cost_currency varchar(255),
        cost_per_install varchar(255),
        is_retargeting bool,
        re_targeting_conversion_type varchar(255),
        country_code varchar(255),
        city varchar(255),
        ip varchar(255),
        wifi bool,
        mac varchar(255),
        "operator" varchar(255),
        carrier varchar(255),
        "language" varchar(255),
        appsflyer_device_id varchar(255),
        advertising_id varchar(255),
        android_id varchar(255),
        customer_user_id varchar(255),
        imei varchar(255),
        platform varchar(255),
        device_brand varchar(255),
        device_model varchar(255),
        os_version varchar(255),
        app_version varchar(255),
        sdk_version varchar(255),
        app_id varchar(255),
        app_name varchar(255),
        event_time timestamp,
        event_name varchar(255),
        event_value varchar(255),
        currency varchar(255),
        validated varchar(255),
        download_time_selected_timezone timestamp,
        click_time_selected_timezone timestamp,
        install_time_selected_timezone timestamp,
        event_time_selected_timezone timestamp,
        selected_currency varchar(255),
        revenue_in_selected_currency varchar(255),
        cost_in_selected_currency varchar(255)
);


CREATE TABLE fbaccounts (
    "id" varchar(255) NOT NULL,
    email varchar(255),
    "name" varchar(255),
    first_name varchar(255),
    last_name varchar(255),
    gender varchar(16),
    locale varchar(16),
    timezone int4,
    birthday date,
    currency char(3),
    link varchar(255),
    PRIMARY KEY ("id")
);

ALTER TABLE fbaccounts ADD COLUMN age_range_min smallint;
ALTER TABLE fbaccounts ADD COLUMN age_range_max smallint;
ALTER TABLE fbaccounts ADD COLUMN install_type varchar(255);
ALTER TABLE fbaccounts ADD COLUMN installed boolean;
ALTER TABLE fbaccounts ADD COLUMN is_shared_login boolean;
ALTER TABLE fbaccounts ADD COLUMN is_verified boolean;
ALTER TABLE fbaccounts ADD COLUMN secure_browsing boolean;
ALTER TABLE fbaccounts ADD COLUMN test_group integer;
ALTER TABLE fbaccounts ADD COLUMN third_party_id varchar(255);
ALTER TABLE fbaccounts ADD COLUMN token_for_business varchar(255);
ALTER TABLE fbaccounts ADD COLUMN updated_time timestamp;
ALTER TABLE fbaccounts ADD COLUMN verified boolean;
ALTER TABLE fbaccounts ADD COLUMN video_upload_limits_length integer;
ALTER TABLE fbaccounts ADD COLUMN video_upload_limits_size bigint;
ALTER TABLE fbaccounts ADD COLUMN viewer_can_send_gift boolean;
ALTER TABLE fbaccounts ADD COLUMN time timestamp;

CREATE INDEX ON fbaccounts (email);

CREATE TABLE bonuses (
    "time" timestamp,
    "type" varchar(255),
    coins int8,
    accountid varchar(255),
    balance int8,
    "level" int4
);

create table fbdevices (
  facebookid varchar(255) REFERENCES fbaccounts("id"),
  deviceindex smallint,
  os varchar(255),
  hardware varchar(255),
  PRIMARY KEY (facebookid, deviceindex)
);

CREATE TABLE logins (
  loginid identity PRIMARY KEY,
  time timestamp,
  accountid varchar(255),
  deviceid varchar(255),
  facebookid varchar(255),
  sessionid varchar(255),
  balance bigint,
  level smallint,
  xp bigint,
  deltadnaid varchar(255),
  deltadnasessionid varchar(255),
  appsflyerid varchar(255),
  ip varchar(255),
  clientver varchar(255),
  serverver varchar(255)
);

CREATE INDEX ON logins(time);
CREATE INDEX ON logins(accountid);
CREATE INDEX ON logins(deviceid);
CREATE INDEX ON logins(facebookid);

CREATE TABLE localizations (
  loginid bigint REFERENCES logins(loginid),
  time timestamp,
  accountid varchar(255),
  systemLanguage varchar(32),
  appLanguage varchar(32),
  countryCode char(2),
  countryName varchar(32),
  vcCode char(3),
  vcSymbol char(3),
  vcFormatWithSymbol varchar(8),
  vcFormatWithoutSymbol varchar(8)
);

CREATE INDEX ON localizations(time);
CREATE INDEX ON localizations(accountid);

CREATE TABLE machines (
  loginid bigint REFERENCES logins(loginid),
  time timestamp,
  accountid varchar(255),
  machine1 varchar(32),
  machine2 varchar(32),
  machine3 varchar(32),
  machines varchar(255)
);

CREATE INDEX ON machines(time);
CREATE INDEX ON machines(accountid);


ALTER TABLE purchases ADD COLUMN sku varchar(255);


ALTER TABLE afevents ADD COLUMN bundle_id varchar(255);
ALTER TABLE afevents ADD COLUMN attributed_touch_time timestamp;
ALTER TABLE afevents ADD COLUMN attributed_touch_type varchar(255);