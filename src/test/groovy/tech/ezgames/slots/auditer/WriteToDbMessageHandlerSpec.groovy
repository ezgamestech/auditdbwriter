package tech.ezgames.slots.auditer

import groovy.json.JsonSlurper
import groovy.sql.GroovyRowResult
import groovy.sql.Sql
import spock.lang.Shared
import spock.lang.Specification

import java.sql.Timestamp
import java.time.*
import java.time.format.DateTimeFormatter
/**
 * Created by idan on 10 Aug 2016.
 */
class WriteToDbMessageHandlerSpec extends Specification {

    Sql sql = Sql.newInstance('jdbc:h2:mem:', 'org.h2.Driver')

    @Shared origTimeZone

    @Shared messageHandler = new WriteToDbMessageHandler( sqlSupplier: {sql} )

    Timestamp msgTime

    void setup() {
        sql.execute(getClass().getResource('db.sql').text)

        msgTime = new Timestamp(
                ZonedDateTime.of(
                        LocalDate.of(2016, Month.SEPTEMBER, 26),
                        LocalTime.MIDNIGHT,
                        ZoneId.of('UTC')
                ).toInstant().toEpochMilli()
        )

    }

    void setupSpec() {
        origTimeZone = TimeZone.default
        TimeZone.default = TimeZone.getTimeZone('UTC')

    }


    void cleanupSpec() {
        TimeZone.default = origTimeZone
    }


    def "Handle a naught version newuser message"() {
        given:
        def msg = [
                type: 'newuser',
                body: [
                        deviceId: 'b5348c058c5d1abb46c346c6d7ec4690',
                        appsFlyerId:'1470845057387-8157209527228877267',
                        deltaDnaId:'91db23bb-a35b-47d2-959c-0a77cb1cfec4',
                        accountId:'4d141b1b-ef79-4368-b070-8012c2c6e0b6',
                        timestamp:1470845116419 as long
                ]
        ]

        when:

        messageHandler.getHandler(msg)(sql, msg.body, msgTime)

        then:
        with (sql.firstRow('select * from accounts')) {
            deviceId == 'b5348c058c5d1abb46c346c6d7ec4690'
            appsFlyerId == '1470845057387-8157209527228877267'
            time == Timestamp.valueOf('2016-08-10 16:05:16.419')
        }
    }

    def "Handle a naught version spins message"() {
        given:
        def msg =  [
                type: 'spin',
                //no version
                body: [
                        accountId:'a1942366-aeb2-449b-b072-46dc492fe055',
                        deviceId:'6387284305922a7f0d61384f7a7765a7',
                        freeSpins:0,
                        megaWin:true,
                        requestId:'18',
                        sessionId:'0250923d-7c7b-4400-a536-00718c7a7dad',
                        totalWin:1380,
                        reel:'BG',
                        bigWin:false,
                        isFreeSpin:false,
                        totalBet:100,
                        balance:14890,
                        gameId:'pixies',
                        bonus:false,
                        timestamp:1470846733212 as long,
                        numLines:50
                ]
        ]

        when:
        def handler = messageHandler.getHandler(msg)

        handler(sql, msg.body, msgTime)


        then:
        with (sql.firstRow('select * from spins')) {
            accountId == 'a1942366-aeb2-449b-b072-46dc492fe055'
            deviceId == '6387284305922a7f0d61384f7a7765a7'
            time == Timestamp.valueOf('2016-08-10 16:32:13.212')
        }
    }

    def "Handle a naught version purchase message"() {
        given:
        Map msg = [
                type: "purchase",
                body: [
                        deviceId: "b650328bda25048d6a5558a654e5e55c",
                        coins:500000,
                        productId: "10_usd_pack",
                        currency: "USD",
                        cost:9.99,
                        receipt: '{"Store":"GooglePlay","TransactionID":"GPA.1366-9247-8913-38464","Payload":"{\\"json\\":\\"{\\\\\\"orderId\\\\\\":\\\\\\"GPA.1366-9247-8913-38464\\\\\\",\\\\\\"packageName\\\\\\":\\\\\\"tech.ezgames.slots\\\\\\",\\\\\\"productId\\\\\\":\\\\\\"15_usd_pack\\\\\\",\\\\\\"purchaseTime\\\\\\":1464686808320,\\\\\\"purchaseState\\\\\\":0,\\\\\\"purchaseToken\\\\\\":\\\\\\"djdbcigifjhocfdmibpccajb.AO-J1OxLHxQg2Jm1yynRtXosy2Dh7VkuiXEPKq3L3rKWtJRI1MK1i7FG8a684l39oRkK7o_HIfcntxUM7Jjt64gMAB6Y4x2LCDb5z9HI1ypB8pGG7E1URok\\\\\\"}\\",\\"signature\\":\\"L8M1qbdIMZ3VVw6uMJdpeFpFI72H0\\\\/LPGRZHGDXR45LynNj2C1CjL5d4OVKOqBG03+lJe6bQoAPc4kFuZo5oLU8+ufWmJy\\\\/hoynYNBJeopnqC6yitim0aWZrXQL8VVt02+tG392SzyKT97v4Fgy9RoYRsNWKJeKzDexpxorypqKfFGlWzUzBWHSpjJud58D4zKRe9PmyyFa48u5OrOS2Fr7qCOwAEfdcwCqBgcY33Zq07xRTZ53QpFFQkURNxjR31okq1ac8HAPYdEMWIhOozLk5wDMM7CWd+HldDhfCMs\\\\/Bdl5\\\\/z6cF1uJbHw1qaDv+ht3le+aBYY\\\\/duFurqCsOlQ==\\"}"}',
                        accountId: "e9926c1f-354f-4b9d-a7ae-555e5ac53863",
                        store: "play.google.com",
                        timestamp:1470824981799
                ]
        ]

        when:

        messageHandler.getHandler(msg)(sql, msg.body, msgTime)

        then:
        with (sql.firstRow('select * from purchases')) {
            coins == 500000
            currency == 'USD'
        }
    }

    def "Handle v1 purchase message"() {
        given:
        Map msg = [
                type:'purchase',
                version:1,
                body:[
                        timestamp:1471208367301,
                        deviceId:'19c3e3241c380a4b6d3ffb2434f12212',
                        request:[
                                store:'play.google.com',
                                itemId:'2_usd_pack',
                                receipt:'{"Store":"GooglePlay","TransactionID":"GPA.1327-1267-2610-48330","Payload":"{\\"json\\":\\"{\\\\\\"orderId\\\\\\":\\\\\\"GPA.1327-1267-2610-48330\\\\\\",\\\\\\"packageName\\\\\\":\\\\\\"tech.ezgames.slots\\\\\\",\\\\\\"productId\\\\\\":\\\\\\"2_usd_pack\\\\\\",\\\\\\"purchaseTime\\\\\\":1471208330484,\\\\\\"purchaseState\\\\\\":0,\\\\\\"purchaseToken\\\\\\":\\\\\\"aidankpjmaacogfnfneeolnb.AO-J1OyaALbWcS-DoaxrNDEZebH4Hdp4z0Hp4yIVfPlxfZfOs74yeeQaFpNfQpQi2NLFX9ynpETbV1pu3qn2goIzbxiT3xZ-9P29BZ-WF3LX0S9_53i6M8M\\\\\\"}\\",\\"signature\\":\\"G8LnZ3T4T85OMfgRAMAv7V9wLNFmPQ4C121Y1OJPDdxYmrepPCT52s8ARKi7zE7zmP2\\\\/wB5Eo+\\\\/UyYOdAOJmexu1Gso10O2LHSs1vqeCGU4qGu7hl0wu2UpfsWd2orDGC6Wn89MM9snNemmrnW6VFkznzGLfGghc7pQFdUl7o4kaXzoJL\\\\/BGEvOZTxtS4RH2\\\\/+AIxVpU7gDZIE+zGusflEVS34r5yzGJVk1To1sEyGy2kDH4HNxCUVi0D64YDvB8FyMGz5NY26JWNAyX2UG\\\\/jHxG9pSF6M4QPh++TqChGXZmlmTYB+Nc5TCJO3CxDb6oH38EaO+\\\\/l0pdwF\\\\/LnAj7aw==\\"}"}',
                                cost:1.99,
                                currency:'USD',
                                context: [
                                    src: 'BANNER',
                                    id: 'OutOfMoney1'
                                ]
                        ],
                        response:[
                                grant:[
                                        coins:60000
                                ],
                                account:[
                                        id:'aaf1f843-f164-4281-8ad5-78cbe39276e5',
                                        facebookId:null,
                                        balance:70000,
                                        level:1,
                                        xp:0,
                                        xpForNextLevel:1000,
                                        bonusForNextLevel:2500
                                ]
                        ]
                ]
        ]

        when:
        messageHandler.getHandler(msg)(sql, msg.body, msgTime)

        then:
        with(sql.firstRow('select * from purchases')) {
            transactionid == 'GPA.1327-1267-2610-48330'
            cost == 1.99
            coins == 60_000
            contextsrc == 'BANNER'
            contextid == 'OutOfMoney1'
        }
    }

    def "Handle v1 purchase message of server 1.4 and higher"() {
        given:
        Map msg = [
                type:'purchase',
                version:1,
                body:[
                        timestamp:1471208367301,
                        deviceId:'19c3e3241c380a4b6d3ffb2434f12212',
                        request:[
                                store:'play.google.com',
                                itemId:'2_usd_pack',
                                receipt: [
                                        store: 'GooglePlay',
                                        transactionId: 'GPA.1327-1267-2610-48330',
                                        payload: '{"json":"{\\"orderId\\":\\"GPA.1327-1267-2610-48330\\",\\"packageName\\":\\"tech.ezgames.slots\\",\\"productId\\":\\"2_usd_pack\\",\\"purchaseTime\\":1471208330484,\\"purchaseState\\":0,\\"purchaseToken\\":\\"aidankpjmaacogfnfneeolnb.AO-J1OyaALbWcS-DoaxrNDEZebH4Hdp4z0Hp4yIVfPlxfZfOs74yeeQaFpNfQpQi2NLFX9ynpETbV1pu3qn2goIzbxiT3xZ-9P29BZ-WF3LX0S9_53i6M8M\\"}","signature":"G8LnZ3T4T85OMfgRAMAv7V9wLNFmPQ4C121Y1OJPDdxYmrepPCT52s8ARKi7zE7zmP2\\/wB5Eo+\\/UyYOdAOJmexu1Gso10O2LHSs1vqeCGU4qGu7hl0wu2UpfsWd2orDGC6Wn89MM9snNemmrnW6VFkznzGLfGghc7pQFdUl7o4kaXzoJL\\/BGEvOZTxtS4RH2\\/+AIxVpU7gDZIE+zGusflEVS34r5yzGJVk1To1sEyGy2kDH4HNxCUVi0D64YDvB8FyMGz5NY26JWNAyX2UG\\/jHxG9pSF6M4QPh++TqChGXZmlmTYB+Nc5TCJO3CxDb6oH38EaO+\\/l0pdwF\\/LnAj7aw=="}'
                                ],
                                cost:1.99,
                                currency:'USD',
                                context: [
                                        src: 'BANNER',
                                        id: 'OutOfMoney1'
                                ]
                        ],
                        response:[
                                grant:[
                                        coins:60000
                                ],
                                account:[
                                        id:'aaf1f843-f164-4281-8ad5-78cbe39276e5',
                                        facebookId:null,
                                        balance:70000,
                                        level:1,
                                        xp:0,
                                        xpForNextLevel:1000,
                                        bonusForNextLevel:2500
                                ]
                        ]
                ]
        ]

        when:
        messageHandler.getHandler(msg)(sql, msg.body, msgTime)

        then:
        with(sql.firstRow('select * from purchases')) {
            transactionid == 'GPA.1327-1267-2610-48330'
            cost == 1.99
            coins == 60_000
            contextsrc == 'BANNER'
            contextid == 'OutOfMoney1'
        }
    }


    def "Handle v1 newuesr message"() {
        given:
        Map msg = [
                type: 'newuser',
                version: 1,
                body: [
                        account: [
                                id: 'accountId',
                                facebookId: 'facebookId',
                                freeSpins: [pixies: 7, vampires: 3 ],
                                periodicBonus: [
                                        secondsUntilReady:  21525
                                ]
                        ],
                        deviceId: 'the deviceId',
                        appsFlyerId: 'the appsFlyer id',
                        deltaDnaId: 'the deltaDNA id',
                        ip: '1.2.3.4',
                        timestamp: System.currentTimeMillis()
                ]
        ]

        when:
        messageHandler.getHandler(msg)(sql, msg.body, msgTime)

        then:
        with (sql.firstRow('select * from accounts')) {
            accountId == 'accountId'
        }
    }

    def "Handle v1 spins message"() {
        given:
        def spinRequest = [
                id: 'requestId',
                reelsName: 'BG',
                bet: 6 as long,
                numLines: 40,
                autoSpin: true,
                maxBet: true,
                force: [
                        win: true,
                        freeSpins: true,
                        wild: true
                ]
        ]

        def account = [
                id: 'accountId',
                balance: 6000 as long,
                level: 3,
                xp: 300 as long,
                facebookId: 'facebookId',
                freeSpins: [pixies: 7, vampires: 3 ],
                periodicBonus: [
                        secondsUntilReady:  21525
                ]
        ]

        def outcome = [
                totalWin: 12345 as long,
                bonus: false,
                freeSpins: 0 as int,
                feedback: 'BIG_WIN'
        ]

        def body = [
                requestId: spinRequest.id,
                sessionId: 'sessionId',
                timestamp: System.currentTimeMillis(),
                deviceId: 'deviceId',
                gameId: 'gameId',
                account: account,
                request: spinRequest,
                outcome: outcome,
                clientIp: '1.2.3.4',
                clientVersion: 'clientVersion',
                serverVersion: 'serverVersion'
        ]

        def msg = [
                type: 'spin',
                version: 1,
                body: body
        ]

        when:
        messageHandler.getHandler(msg)(sql, body, msgTime)

        then:
        with (sql.firstRow('select * from spins')) {
            balance == 6000
            level == 3
            xp == 300
            numLines == 40
            bigWin == true
            megaWin == false
            win == 12345
            clientip == '1.2.3.4'
            clientver == 'clientVersion'
            serverver == 'serverVersion'
            autospin == true
            maxbet == true
            force == 'W,FS,WL'
        }
    }

    def "Handle afevent message"() {
        given:
        //a real AF Event
        def afeventsJson= '''
{
	"type": "afevent",
	"version": 1,
	"body": {
		"device_model": "SAMSUNG-SM-J320A",
		"fb_adgroup_id": "23842502623540507",
		"click_time_selected_timezone": "2016-08-14 13:58:13.000+0000",
		"download_time_selected_timezone": "2016-08-14 14:03:00.000+0000",
		"download_time": "2016-08-14 14:03:00",
		"operator": "",
		"af_keywords": null,
		"click_time": "2016-08-14 13:58:13",
		"agency": null,
		"af_adset": null,
		"ip": "107.77.216.42",
		"cost_per_install": null,
		"fb_campaign_id": "23842502623360507",
		"imei": null,
		"is_retargeting": false,
		"af_cost_currency": null,
		"app_name": "SlotsVille - Free Slot Games",
		"re_targeting_conversion_type": null,
		"af_channel": null,
		"android_id": null,
		"city": "Kokhav Ya'Ir",
		"af_sub1": null,
		"af_sub2": null,
		"event_value": "{\\"game_name\\":\\"Pixie Luck\\",\\"game\\":\\"pixies\\"}",
		"af_sub3": null,
		"fb_adset_name": "US - W - 35+",
		"af_sub4": null,
		"customer_user_id": "f82a63e6-782f-4582-8851-b01c6d6af1c3",
		"mac": null,
		"af_sub5": null,
		"install_time_selected_timezone": null,
		"campaign": null,
		"event_name": "enter_machine_first_time",
		"event_time_selected_timezone": "2016-08-14 14:09:24.148+0000",
		"currency": "",
		"af_cost_model": null,
		"af_c_id": null,
		"install_time": "2016-08-14 14:05:37",
		"fb_adgroup_name": "Default Name - App Installs - Image 1",
		"event_time": "2016-08-14 14:09:24",
		"platform": "android",
		"sdk_version": "4.1",
		"appsflyer_device_id": "1471183417265-6578767615491344738",
		"selected_currency": "USD",
		"af_adset_id": null,
		"wifi": false,
		"advertising_id": "2063145e-c079-4869-8691-5e6f550092d8",
		"media_source": "Facebook Ads",
		"country_code": "US",
		"http_referrer": null,
		"fb_campaign_name": "App Installs - AUG 9",
		"click_url": null,
		"carrier": "AT&T",
		"language": "English",
		"af_cost_value": null,
		"app_id": "tech.ezgames.slots",
		"app_version": "1.0.72",
		"attribution_type": "regular",
		"af_ad_type": null,
		"af_siteid": null,
		"os_version": "6.0.1",
		"af_ad_id": null,
		"af_ad": null,
		"fb_adset_id": "23842502623410507",
		"device_brand": "samsung",
		"revenue_in_selected_currency": null,
		"event_type": "in-app-event"
	}
}
'''


        def msg = new JsonSlurper().parseText(afeventsJson)

        when:
        when:
        messageHandler.getHandler(msg)(sql, msg.body, msgTime)
        GroovyRowResult row = sql.firstRow('select * from afevents')

        then:
        with(row) {
            //verify some values
            click_time_selected_timezone == Timestamp.valueOf('2016-08-14 13:58:13.000')
            wifi == false
            language == 'English'
            city == 'Kokhav Ya\'Ir'

            af_ad_id == null
            af_ad == null

        }
    }


    def "Handle fbaccount message"() {
        given:

        def accountId = 'weiejafdsf'
        def facebookId = 'asklfeifaff'



        String msgJson = """
{
  "type": "fbaccount",
  "version": 1,
  "body": {
    "account": {
      "id": "$accountId",
      "facebookId": "$facebookId",
      "balance": 10000,
      "level": 1,
      "xp": 0,
      "xpForNextLevel": 1000,
      "bonusForNextLevel": 2500
    },
    "deviceId": "test-idan",
    "fb": {
      "id": "$facebookId",
      "email": "someone@example.com",
      "birthday": "10/27/1979",
      "first_name": "Some",
      "last_name": "O'neil",
      "name": "Some O'neil",
      "gender": "male",
      "age_range" : {
        "min" : 13,
        "max" : 20
      },
      "locale": "en_US",
      "timezone": 3,
      "currency": {
        "currency_offset": 100,
        "usd_exchange": 0.26052642,
        "usd_exchange_inverse": 3.838382303,
        "user_currency": "ILS"
      },
      "link": "https://www.facebook.com/app_scoped_user_id/1544870245817773/",
      "friends": {
        "data": [{
          "id": "0987654321",
          "name": "John Doe"
        }],
        "paging": {
          "cursors": {
            "after": "QVFIUlAtUWVpYU1VOC1iUDN4cjQxejRKaTN0ZAmlZAX0h5SUk2UDRmXzBmYUJGWVdHS09GRDVMcUM3ODFVbl9VVTk2RW8ZD",
            "before": "QVFIUlAtUWVpYU1VOC1iUDN4cjQxejRKaTN0ZAmlZAX0h5SUk2UDRmXzBmYUJGWVdHS09GRDVMcUM3ODFVbl9VVTk2RW8ZD"
          }
        },
        "summary": {
          "total_count": 4
        }
      }
    }
  }
}
"""
        and: 'An account exists in accounts table'
        sql.execute("insert into accounts (accountid) values ($accountId);")

        when:
        def msgMap = new JsonSlurper().parseText(msgJson)

        messageHandler.getHandler(msgMap)(sql, msgMap.body as Map, msgTime)

        then:
        with (sql.firstRow('select * from fbaccounts')) {
            id == facebookId
            time as String == '2016-09-26 00:00:00.0'
            email == 'someone@example.com'
            last_name == 'O\'neil'
            birthday as String == '1979-10-27'
            gender == 'male'
            locale == 'en_US'
            timezone == 3
            currency == 'ILS'
            link == 'https://www.facebook.com/app_scoped_user_id/1544870245817773/'
            age_range_min == 13
        }

        and:

        with (sql.firstRow("select facebookid from accounts where accountid = $accountId")) {
            facebookid == facebookId
        }
    }

    //TODO add bonus test
    def "Handle fbaccount message with 2nd addition of columns"() {
        given:

        def accountId = 'qwerqewr'
        def facebookId = 'zxcvzxcv'



        String msgJson = """
{
  "type": "fbaccount",
  "version": 1,
  "body": {
    "account": {
      "id": "$accountId",
      "facebookId": "$facebookId",
      "balance": 10000,
      "level": 1,
      "xp": 0,
      "xpForNextLevel": 1000,
      "bonusForNextLevel": 2500
    },
    "deviceId": "test-idan",
    "fb": {
      "id": "$facebookId",
      "name": "James Bond",
      "first_name": "James",
      "last_name": "Bond",
      "age_range": {
        "min": 21
      },
      "context": {
        "mutual_friends": {
          "data": [
          ],
          "summary": {
            "total_count": 41
          }
        },
        "mutual_likes": {
          "data": [
          ],
          "summary": {
            "total_count": 91
          }
        },
        "id": "dXNlcl9jb250ZAXh0OgGQoTjsJi3h3A2ev900bVZAMktVj5ofRZAWy2dopu1MVA0nO6Q3kMFaj3E997cE0pxMV2ctA7GXA8QjtqZC0zgw2UIbdm2f41i90N9pi0H0Obuw9QZD"
      },
      "currency": {
        "currency_offset": 100,
        "usd_exchange": 1,
        "usd_exchange_inverse": 1,
        "user_currency": "USD"
      },
      "devices": [
        {
          "os": "Android"
        }
      ],
      "email": "jamesbond@example.com",
      "gender": "female",
      "install_type": "UNKNOWN",
      "installed": true,
      "is_shared_login": false,
      "is_verified": false,
      "link": "https://www.facebook.com/app_scoped_user_id/$facebookId/",
      "locale": "en_US",
      "name_format": "{first} {last}",
      "payment_pricepoints": {
        "mobile": [
          {
            "credits": 10,
            "local_currency": "USD",
            "user_price": "1.00"
          },
          {
            "credits": 20,
            "local_currency": "USD",
            "user_price": "2.00"
          },
          {
            "credits": 30,
            "local_currency": "USD",
            "user_price": "3.00"
          },
          {
            "credits": 40,
            "local_currency": "USD",
            "user_price": "3.99"
          },
          {
            "credits": 50,
            "local_currency": "USD",
            "user_price": "5.00"
          },
          {
            "credits": 60,
            "local_currency": "USD",
            "user_price": "5.99"
          },
          {
            "credits": 70,
            "local_currency": "USD",
            "user_price": "6.99"
          },
          {
            "credits": 100,
            "local_currency": "USD",
            "user_price": "9.99"
          },
          {
            "credits": 200,
            "local_currency": "USD",
            "user_price": "19.99"
          }
        ]
      },
      "security_settings": {
        "secure_browsing": {
          "enabled": true
        }
      },
      "test_group": 6,
      "third_party_id": "nqYNsqsxjGqVrsxym3G3taCeBLA",
      "timezone": -5,
      "token_for_business": "AczCMp4cwG1-Fg3A",
      "updated_time": "2016-05-23T03:30:17+0000",
      "verified": true,
      "video_upload_limits": {
        "length": 14460,
        "size": 28633115306
      },
      "viewer_can_send_gift": false
    }
  }
}
"""
        and: 'An account exists in accounts table'
        sql.execute("insert into accounts (accountid) values ($accountId);")

        when:
        def msgMap = new JsonSlurper().parseText(msgJson)

        messageHandler.getHandler(msgMap)(sql, msgMap.body as Map, msgTime)

        GroovyRowResult row2 = sql.firstRow("select * from fbaccounts where \"id\" = $facebookId")
        println row2
        then:
        with (row2) {
            id == facebookId
            email == 'jamesbond@example.com'
            last_name == 'Bond'
            video_upload_limits_size == 28633115306
            !viewer_can_send_gift
        }

        GroovyRowResult row1 = sql.firstRow("select facebookid from accounts where accountid = $accountId")
        and:

        with (row1) {
            facebookid == facebookId
        }
        println row1

        GroovyRowResult row = sql.firstRow("select * from fbdevices where facebookid = $facebookId")
        with (row) {
            os == 'Android'
        }

        println row
    }

    def "Parse facebook timestamp" () {
        given:
        String text = '2016-05-23T03:30:17+0000'

        def formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME

        when:

        text = text.replaceAll(/([+-]\d\d)(\d\d)$/, '$1:$2')

        def datetime = ZonedDateTime.parse(text)
        println datetime

        Timestamp ts = new Timestamp(datetime.toInstant().toEpochMilli())
        println ts

        then:
        noExceptionThrown()
    }


    static def loginMsg = '''
{
	"type": "login",
	"version": 1,
	"body": {
		"accountId": "58de835c-465f-454d-ab53-0c0509f340df",
		"deviceId": "4a47cdeadfc1117fae1f5173f690f259",
		"ip": "128.177.161.140",
		"clientVersion": "1.1.12",
		"serverVersion": "1.2.195",
		"request": {
			"deltadna": {
				"id": "3c147597-849d-45d4-9c22-579c5caee613",
				"sessionId": "ba203cf0-18a0-491e-b2ad-d3046c795c2c"
			},
			"appsflyer": {
				"id": "1473311876310-8091926645780287345"
			},
			"facebook": {
				"id": null,
				"accessToken": "adsflkjjdfsalkjasfdlkj"
			},
			"systemLanguage": "en",
			"localizationLanguage": "en"
		},
		"response": {
			"session": {
				"id": "4ba9ddbd-e413-4b9d-afef-b48b89126b1a"
			},
			"debug": {
				"hostname": "s1",
				"timestamp": "1473870994739",
				"serverVersion": "1.2.195"
			},
			"dailyMissions": {
				"secondsUntilReady": 0,
				"missions": [{
					"type": "auto-spin",
					"progress": {
						"max": 20,
						"value": 0
					}
				}, {
					"type": "three-of-a-kind",
					"progress": {
						"max": 5,
						"value": 5
					}
				}, {
					"type": "different-games",
					"progress": {
						"max": 3,
						"value": 3
					},
					"gamesPlayed": ["egypt", "wildwest", "witches"]
				}],
				"grant": {
					"coins": 3000
				}
			},
			"periodicBonus": {
				"secondsUntilReady": 0,
				"grant": {
					"coins": 1500
				}
			},
			"lobby": {
				"games": [{
					"id": "savanna",
					"order": 0,
					"unlockLevel": 1,
					"href": "/games/savanna"
				}, {
					"id": "wildwest",
					"order": 1,
					"unlockLevel": 1,
					"href": "/games/wildwest"
				}, {
					"id": "witches",
					"order": 2,
					"unlockLevel": 3,
					"href": "/games/witches"
				}, {
					"id": "egypt",
					"order": 3,
					"unlockLevel": 5,
					"href": "/games/egypt"
				}, {
					"id": "pixies",
					"order": 4,
					"unlockLevel": 7,
					"href": "/games/pixies"
				}, {
					"id": "vampires",
					"order": 5,
					"unlockLevel": 10,
					"href": "/games/vampires"
				}, {
					"id": "unknown",
					"order": 6,
					"unlockLevel": 15,
					"href": "/games/unknown"
				}, {
					"id": "unknown",
					"order": 7,
					"unlockLevel": 20,
					"href": "/games/unknown"
				}, {
					"id": "unknown",
					"order": 8,
					"unlockLevel": 25,
					"href": "/games/unknown"
				}, {
					"id": "unknown",
					"order": 9,
					"unlockLevel": 30,
					"href": "/games/unknown"
				}, {
					"id": "unknown",
					"order": 10,
					"unlockLevel": 35,
					"href": "/games/unknown"
				}, {
					"id": "unknown",
					"order": 11,
					"unlockLevel": 40,
					"href": "/games/unknown"
				}, {
					"id": "unknown",
					"order": 12,
					"unlockLevel": 45,
					"href": "/games/unknown"
				}, {
					"id": "unknown",
					"order": 13,
					"unlockLevel": 50,
					"href": "/games/unknown"
				}]
			},
			"account": {
				"id": "58de835c-465f-454d-ab53-0c0509f340df",
				"facebookId": "1783622818593314",
				"balance": 227,
				"level": 8,
				"xp": 119030,
				"xpForNextLevel": 120000,
				"bonusForNextLevel": 2500
			},
			"Localization": {
				"location": {
					"continent": {
						"name": "North America",
						"isoCode": "NA"
					},
					"country": {
						"name": "United States",
						"isoCode": "US"
					}
				},
				"virtualCurrencyWithSymbolFormat": "$#,##0",
				"virtualCurrencyWithoutSymbolFormat": "#,##0"
			}
		}
	}
}
'''

    def "handle login msg"() {
        given:
        Map msg = new JsonSlurper().parseText(loginMsg) as Map

        when:
        messageHandler.getHandler(msg)(sql, msg.body, msgTime)
        def loginsFirstRow = sql.firstRow('select * from logins')

        def loginid = loginsFirstRow.loginid

        println "loginid = $loginid"

        then:

        with (loginsFirstRow) {
            loginid != null
            time as String == '2016-09-26 00:00:00.0'
            accountid == '58de835c-465f-454d-ab53-0c0509f340df'
            deviceId == '4a47cdeadfc1117fae1f5173f690f259'
        }

        with (sql.firstRow("select * from localizations where loginid = $loginid")) {
            accountid == '58de835c-465f-454d-ab53-0c0509f340df'
            systemlanguage == 'en'
            countryCode == 'US'
            vcCode == 'USD'
            vcSymbol == '$'
        }

        with (sql.firstRow("select * from machines where loginid = $loginid")) {
            accountid == '58de835c-465f-454d-ab53-0c0509f340df'
            machine1 == 'savanna'
            machine2 == 'wildwest'
            machine3 == 'witches'
            machines == 'savanna,wildwest,witches,egypt,pixies,vampires'
        }

    }

    def "multiple logins with same l10n and machines don't add additional records"() {
        given:
        Map msg = new JsonSlurper().parseText(loginMsg) as Map

        Map msg2 = new JsonSlurper().parseText(loginMsg) as Map

        msg2.body.response.account.balance = 1000
        msg2.body.response.account.level = 9
        msg2.body.response.account.xp = 100

        when:
        messageHandler.getHandler(msg)(sql, msg.body, msgTime)
        messageHandler.getHandler(msg2)(sql, msg2.body, msgTime + 1)

        then:
        sql.rows('SELECT * FROM logins').size() == 2
        sql.rows('SELECT * FROM localizations').size() == 1
        sql.rows('SELECT * FROM machines').size() == 1
    }

    def "multiple logins with same l10n and different machines don't add to l10ns but do add to machines"() {
        given:
        Map msg = new JsonSlurper().parseText(loginMsg) as Map

        Map msg2 = new JsonSlurper().parseText(loginMsg) as Map

        println msg2
        msg2.body.response.account.balance = 1000
        msg2.body.response.account.level = 9
        msg2.body.response.account.xp = 100
        msg2.body.response.lobby.games[6].id = 'dolphins'

        println msg2

        when:
        messageHandler.getHandler(msg)(sql, msg.body, msgTime)
        messageHandler.getHandler(msg2)(sql, msg2.body, msgTime + 1)

        then:
        sql.rows('SELECT * FROM logins').size() == 2
        sql.rows('SELECT * FROM localizations').size() == 1
        sql.rows('SELECT * FROM machines').size() == 2
    }


    def "multiple logins with diff l10n and same machines don't add to machines but do add to l10ns"() {
        given:
        Map msg = new JsonSlurper().parseText(loginMsg) as Map

        Map msg2 = new JsonSlurper().parseText(loginMsg) as Map

        println msg2
        msg2.body.response.account.balance = 1000
        msg2.body.response.account.level = 9
        msg2.body.response.account.xp = 100
        msg2.body.response.Localization.location.country.isoCode = 'IL'

        println msg2

        when:
        messageHandler.getHandler(msg)(sql, msg.body, msgTime)
        messageHandler.getHandler(msg2)(sql, msg2.body, msgTime + 1)

        then:
        sql.rows('SELECT * FROM logins').size() == 2
        sql.rows('SELECT * FROM localizations').size() == 2
        sql.rows('SELECT * FROM machines').size() == 1
    }

    def "Parse X-Aws-Sqsd-First-Received-At value"() {
        given:
        def t = '2014-10-21T11:01:06Z'

        when:
        ZonedDateTime zdt = ZonedDateTime.parse(t)
        Timestamp timestamp = new Timestamp(zdt.toInstant().toEpochMilli())

        then:
        timestamp.toString() == '2014-10-21 11:01:06.0'
    }


    def "Handle new AppsFlyer event message from 2016-12-06"() {
        given:
        Map msg =new JsonSlurper().parse(this.class.getResourceAsStream('afevent-20161206.json'))

        when:
        messageHandler.getHandler(msg)(sql, msg.body, msgTime)

        GroovyRowResult row = sql.firstRow('select * from afevents')

        println row.attributed_touch_time

        then:
        with (row) {
            attributed_touch_time != null
        }

    }
}
