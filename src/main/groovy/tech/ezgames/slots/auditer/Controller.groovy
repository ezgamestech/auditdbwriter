package tech.ezgames.slots.auditer

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

import java.sql.Timestamp
import java.time.ZonedDateTime

/**
 * Created by idan on 04 Aug 2016.
 */
@RestController
class Controller {

    static Logger log = LoggerFactory.getLogger(Controller)

    @Autowired
    MessageHandler messageHandler

    @RequestMapping (path = '/readFromQueue', method = RequestMethod.POST)
    def readFromQueue(@RequestBody Map message, @RequestHeader('X-Aws-Sqsd-First-Received-At') String receivedAt) {
        log.info("Received message $message, receivedAt $receivedAt")

        Timestamp msgTime
        try {
            ZonedDateTime zdt = ZonedDateTime.parse(receivedAt)
            msgTime = new Timestamp(zdt.toInstant().toEpochMilli())
        } catch (Exception e) {
            log.error("Error parsing msg receivedAt time $receivedAt", e)
        }

        messageHandler.handleMessage(message, msgTime)
    }



}
