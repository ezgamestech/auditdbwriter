package tech.ezgames.slots.auditer

import java.sql.Timestamp

/**
 * Created by idan on 10 Aug 2016.
 */
interface MessageHandler {

    def handleMessage(Map message, Timestamp msgTime)
}