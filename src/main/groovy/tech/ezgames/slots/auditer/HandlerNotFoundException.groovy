package tech.ezgames.slots.auditer

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus
/**
 * Created by idan on 14 Aug 2016.
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
class HandlerNotFoundException extends RuntimeException {

    def aMessage

    @Override
    String getMessage() {
        "No handler found for message $aMessage"
    }
}
