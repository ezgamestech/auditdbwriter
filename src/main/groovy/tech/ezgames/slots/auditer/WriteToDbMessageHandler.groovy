package tech.ezgames.slots.auditer

import groovy.json.JsonSlurper
import groovy.sql.DataSet
import groovy.sql.GroovyRowResult
import groovy.sql.Sql
import org.apache.log4j.Logger
import org.postgresql.util.PSQLException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import tech.ezgames.slots.auditer.db.SqlSupplier

import java.sql.Date
import java.sql.Timestamp
import java.time.LocalDate
import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.time.temporal.TemporalQueries
/**
 * Created by idan on 10 Aug 2016.
 */
@Component
class WriteToDbMessageHandler implements MessageHandler {

    static Logger log = Logger.getLogger(WriteToDbMessageHandler)

    @Autowired
    SqlSupplier sqlSupplier

    public handleMessage(Map message, Timestamp msgTime) {
        def handler = getHandler(message)

        if (handler) {
            executeSql(message.body, handler, msgTime)
        } else {
            throw new HandlerNotFoundException(aMessage: message)
        }
    }

    def getHandler(Map message) {
        def version = message.version
        switch (message.type) {
            case 'spin' :
                if (!version)
                    return this.&handleSpin0
                return this.&handleSpin
            case 'newuser' :
                if (!version)
                    return this.&handleNewUser0
                return this.&handleNewUser
            case 'purchase' :
                if (!version)
                    return this.&handlePurchase0
                return this.&handlePurchase
            case 'afevent' :
                return this.&handleAfEvent
            case 'fbaccount' :
                return this.&handleFbAccount
            case 'bonus' :
                return this.&handleBonus
            case 'login' :
                return this.&handleLogin
            default:
                log.warn("Unknown message type $message.type")
                return null
        }
    }


    def handleBonus(Sql sql, def body, Timestamp msgTime) {
        Timestamp timestamp = new Timestamp(body.time as long)

        sql.execute("""
        INSERT INTO bonuses (time, type, coins, accountid, deviceid, balance, level)
        VALUES ($timestamp, $body.type, $body.coins,
                $body.account.id, $body.deviceId, $body.account.balance, $body.account.level);
""")
    }

    static Timestamp getFacebookUpdatedTime(String updated_time) {
        if (!updated_time)
            return null
        //Java parses expect zone offset to be formatted as hh:mm with the colon
        def fixedUpdatedTime = updated_time.replaceAll(/([+-]\d\d)(\d\d)$/, '$1:$2')
        ZonedDateTime zdt = ZonedDateTime.parse(fixedUpdatedTime)
        new Timestamp(zdt.toInstant().toEpochMilli())
    }

    private static getFacebookBirthday(def fb) {
        def birthday = null
        if (fb.birthday) {
            LocalDate localBirthdate = DateTimeFormatter.ofPattern('MM/dd/yyyy').parse(fb.birthday as String, TemporalQueries.localDate())
            birthday = new Date(localBirthdate.atTime(0, 0).toInstant(ZoneOffset.UTC).toEpochMilli())
        }
        birthday
    }

    def handleFbAccount(Sql sql, Map<String, String> body, Timestamp msgTime) {
        Timestamp time = body.timestamp ? new Timestamp(body.timestamp) : msgTime

        def account = body.account
        def fb = body.fb
        def age_range = fb.age_range

        def birthday = getFacebookBirthday(fb)

        DataSet fbaccounts = sql.dataSet('fbaccounts')

        //run in a transaction - so both succeed or fail together
        sql.withTransaction {
            //id and name are wrapped because they're reserved SQL words
            //see https://www.postgresql.org/docs/current/static/sql-keywords-appendix.html
            fbaccounts.add(
                    time: time,
                    '"id"': fb.id, email: fb.email, '"name"': fb.name,
                    first_name: fb.first_name, last_name: fb.last_name,
                    birthday: birthday,
                    gender: fb.gender, locale: fb.locale, timezone: fb.timezone,
                    currency: fb?.currency?.user_currency, link: fb.link,
                    //These fields were added later
                    age_range_min: age_range?.min, age_range_max: age_range?.max,
                    install_type: fb.install_type,
                    installed: fb.installed,
                    is_shared_login: fb.is_shared_login,
                    is_verified: fb.is_verified,
                    secure_browsing: fb.security_settings?.secure_browsing?.enabled,
                    test_group: fb.test_group,
                    third_party_id: fb.third_party_id,
                    token_for_business: fb.token_for_business,
                    updated_time: getFacebookUpdatedTime(fb.updated_time),
                    verified: fb.verified,
                    video_upload_limits_length: fb.video_upload_limits?.length,
                    video_upload_limits_size: fb.video_upload_limits?.size,
                    viewer_can_send_gift: fb.viewer_can_send_gift
            )
            sql.executeUpdate("UPDATE accounts SET facebookid=$fb.id WHERE accountid=$account.id")
        }

        if (fb.devices) {
            DataSet fbdevices = sql.dataSet('fbdevices')
            fb.devices.eachWithIndex { device, idx ->
                fbdevices.add(facebookid: fb.id, deviceindex: idx, os: device.os, hardware: device.hardware)
            }
        }
    }

    def handleAfEvent(Sql sql, Map<String, String> body, Timestamp msgTime) {
        List columns = []
        List values = []
        List qmarks = []

        body.each {k,v ->

            if (v == null)
                return //skip over null values

            def value = v
            if (k.endsWith('time')) {
                Timestamp ts = Timestamp.valueOf(v)
                value = ts
            } else if (k.endsWith('timezone')) {
                ZonedDateTime zdt = ZonedDateTime.parse(v, DateTimeFormatter.ofPattern('yyyy-MM-dd HH:mm:ss.SSSZ')) //the pattern AF use
                Timestamp ts = new Timestamp(zdt.toInstant().toEpochMilli())
                value = ts
            } else if (['wifi', 'is_retargeting'].contains(k)) {
                value = v as boolean
            }

            def key = k
            switch (k) {
            //these are reserved words in (P)SQL so we need to wrap the column names with double quotes
                case 'operator':
                case 'language':
                    key = "\"$k\""
                    break
                default:
                    key = k
                    break
            }

            columns << key
            qmarks << '?'
            values << value
        }

        GString sqlInsertStatement = "INSERT INTO AfEvents (${columns.join(',')}) VALUES (${qmarks.join(',')});"
        log.info("executing $sqlInsertStatement with values = $values")

        sql.execute(sqlInsertStatement.toString(), values)

    }

    def handlePurchase(Sql sql, def body, Timestamp msgTime) {
        final String transactionId
        if (body.request.receipt instanceof String) { //to support server ver 1.3.213
            def tmp = new JsonSlurper().parseText(body.request.receipt)
            transactionId = tmp.TransactionID
        }
        else { //in ver 1.4 the receipt was changed from a json string to a UnityWrappedReceipt object
            transactionId = body.request.receipt.transactionId
        }

        Timestamp timestamp = new Timestamp(body.timestamp as long)

        def context = [src: null, id: null]
        if (body.request.context) {
            context = body.request.context
        }

        sql.execute("""
            INSERT INTO Purchases (time, accountId, deviceId,
                store, sku, productId, transactionId,
                "cost", currency, coins,
                contextsrc, contextid)
            VALUES (?, ?, ?,
                    ?, ?, ?, ?,
                    ?, ?, ?,
                    ?, ?)
""",
                [timestamp, body.response.account.id, body.deviceId,
                 body.request.store, body.request.sku, body.request.itemId, transactionId,
                 body.request.cost as double, body.request.currency, body.response.grant.coins as long,
                 context.src, context.id == '' ? null : context.id]
        );
    }

    def handlePurchase0(Sql sql, def body, Timestamp msgTime) {

        def receiptTxt = body.receipt

        JsonSlurper slurper = new JsonSlurper()
        def receipt = slurper.parseText(receiptTxt)
        String transactionId = receipt.TransactionID

        Timestamp timestamp = new Timestamp(body.timestamp as long)

        sql.execute("""
INSERT INTO Purchases (time, accountId, deviceId, store,
productId, transactionId, "cost", currency, coins)
VALUES ($timestamp, $body.accountId, $body.deviceId, $body.store,
$body.productId, $transactionId, ${body.cost as double},
$body.currency, ${body.coins as long});
""")
    }

    def handleNewUser(Sql sql, def body, Timestamp msgTime) {
        Timestamp timestamp = new Timestamp(body.timestamp)

        sql.execute("""
            INSERT INTO Accounts (accountId, deviceId, appsflyerid, deltadnaid, ip, time)
            VALUES (?, ?, ?, ?, ?, ?);""",
                [body.account.id, body.deviceId, body.appsFlyerId, body.deltaDnaId, body.ip, timestamp]);
    }

    def handleNewUser0(Sql sql, def body, Timestamp msgTime) {
//        getSql.execute("""
//CREATE TABLE IF NOT EXISTS Accounts
//(
//    accountId varchar(255) NOT NULL,
//    deviceId varchar(255),
//    appsFlyerId varchar(255),
//    deltaDnaId varchar(255),
//    timestamp timestamp,
//    PRIMARY KEY (accountId)
//)
//""")
        Timestamp timestamp = new Timestamp(body.timestamp)

        sql.execute("""
INSERT INTO Accounts (accountId, deviceId, appsflyerid, deltadnaid, time)
VALUES ($body.accountId, $body.deviceId, $body.appsFlyerId, $body.deltaDnaId, $timestamp);
""")
    }

    def executeSql(def body, Closure closure, Timestamp msgTime) {
        Sql sql
        try {
            sql = sqlSupplier.getSql()
            closure.call(sql, body, msgTime)
            log.info('Added record to audit db')
        } catch (PSQLException pe) {
            if ('23505' == pe.getSQLState()) {
                log.warn('Not inserting duplicate key value', pe)
            } else {
                handleException(pe)
            }
        } catch (Exception e) {
            handleException(e)
        } finally {
            if (sql != null) {
                sql.close()
            }
        }
    }

    def handleException(Exception e) {
        log.error('Failed to write to db', e)
        throw e
    }

    static String getForceFlagsString(def body) {
        def list = []

        if (body?.request?.force?.win)
            list << 'W'
        if (body?.request?.force?.freeSpins)
            list << 'FS'
        if (body?.request?.force?.bigWin)
            list << 'BW'
        if (body?.request?.force?.megaWin)
            list << 'MW'
        if (body?.request?.force?.bonus)
            list << 'BN'
        if (body?.request?.force?.wild)
            list << 'WL'

        if (list.isEmpty())
            return null

        return list.join(',')
    }

    def handleSpin(Sql sql, def body, Timestamp msgTime) {
        Timestamp timestamp = new Timestamp(body.timestamp as long)

        long bet = body.request.bet as long
        long numLines = body.request.numLines as long

        DataSet spins = sql.dataSet('spins')

        spins.add(
                requestId: body.requestId,
                sessionId: body.sessionId,
                time: timestamp,
                accountId: body.account.id,
                deviceId: body.deviceId,
                balance: body.account.balance,
                xp: body.account.xp,
                totalXp: body.account.totalXp,
                '\"level\"': body.account.level,
                gameId: body.gameId,
                reel: body.request.reelsName,
                totalBet: (bet * numLines),
                numLines: numLines,
                isFreeSpin: (body.request.reelsName == 'FG'),
                bonus: body.outcome.bonus,
                bigwin: body.outcome.feedback == 'BIG_WIN',
                megawin: body.outcome.feedback == 'MEGA_WIN',
                win: body.outcome.totalWin,
                clientip: body.clientIp,
                clientver: body.clientVersion,
                serverver: body.serverVersion,
                autospin: body.request.autoSpin,
                maxbet: body.request.maxBet,
                force: getForceFlagsString(body)
        )
    }

    void handleSpin0(Sql sql, def body, Timestamp msgTime) {
//        getSql.execute("""
//CREATE TABLE IF NOT EXISTS Spins
//(
//    requestId varchar(255) NOT NULL,
//    sessionId varchar(255),
//    timestamp timestamp,
//    accountId varchar(255) NOT NULL,
//    deviceId varchar(255) NOT NULL,
//    balance bigint,
//    xp bigint,
//    level integer,
//    gameId varchar(255),
//    reel varchar(255),
//    totalBet integer,
//    numLines integer,
//    isFreeSpin boolean,
//    bonus boolean,
//    bigWin boolean,
//    megaWin boolean,
//    PRIMARY KEY (requestId)
//)
//""")

        Timestamp timestamp = new Timestamp(body.timestamp)

        sql.execute("""
        INSERT INTO Spins (requestId, sessionId, time,
            accountId, deviceId, balance, gameId, reel, totalBet,
            numLines, isFreeSpin, bonus, bigWin, megaWin)
        VALUES ($body.requestId, $body.sessionId, $timestamp,
            $body.accountId, $body.deviceId, $body.balance, $body.gameId, $body.reel, $body.totalBet,
            $body.numLines, $body.isFreeSpin, $body.bonus, $body.bigWin, $body.megaWin);
""")
    }

    private static final def VC_SYMBOL_TO_CODE = ['$':'USD',
                                                  '€':'EUR',
                                                  '$b':'BOB',
                                                  'R$':'BRL',
                                                  '¥':'JPY',
                                                  '₡':'CRC',
                                                  '£':'GBP',
                                                  'kr':'ISK',
                                                  'kn':'HRK',
                                                  'Ft':'HUF',
                                                  'Rp':'IDR',
                                                  '₪':'ILS',
                                                  'RM':'MYR',
                                                  'zł':'PLN',
                                                  '₽':'RUB',
                                                  '฿':'THB',
                                                  '₺':'TRY',
                                                  'NT$':'TWD',
                                                  '₴':'UAH',
                                                  '$U':'UYU',
                                                  'S':'ZAR']

    def handleLogin(Sql sql, def body, Timestamp msgTime) {
        Timestamp time = body.timestamp ? new Timestamp(body.timestamp) : msgTime

        DataSet logins = sql.dataSet('logins')

        def request = body.request
        def response = body.response
        def account = response.account

        def result = sql.executeInsert('''
            INSERT INTO logins (
                time,
                accountid,
                deviceid,
                facebookid,
                sessionid,
                balance,
                level,
                xp,
                deltadnaid,
                deltadnasessionid,
                appsflyerid,
                ip,
                clientver,
                serverver)
              VALUES (
                ?,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?)''',
                [time,
                 account.id,
                 body.deviceId,
                 request.facebook?.id,
                 response.session.id,
                 account.balance,
                 account.level,
                 account.xp,
                 request.deltadna?.id,
                 request.deltadna?.sessionId,
                 request.appsflyer?.id,
                 body.ip,
                 body.clientVersion,
                 body.serverVersion])

        int loginid = result[0][0]

        addLocalizationRecord(sql, loginid, time, account, request, response)

        addMachinesRecord(sql, loginid, time, account, request, response)

    }

    private void addLocalizationRecord(Sql sql, int loginid, Timestamp time, account, request, response) {

        String vcFormatWithSymbol = response.Localization?.virtualCurrencyWithSymbolFormat
        String vcFormatWithoutSymbol = response.Localization?.virtualCurrencyWithoutSymbolFormat
        String vcSymbol = vcFormatWithSymbol - vcFormatWithoutSymbol

        Map<String, Object> record = [
                accountid            : account.id,
                systemLanguage       : request.systemLanguage,
                appLanguage          : request.localizationLanguage,
                countryCode          : response.Localization?.location?.country?.isoCode,
                countryName          : response.Localization?.location?.country?.name,
                vcCode               : vcSymbol ? VC_SYMBOL_TO_CODE[vcSymbol] : null,
                vcSymbol             : vcSymbol,
                vcFormatWithSymbol   : vcFormatWithSymbol,
                vcFormatWithoutSymbol: vcFormatWithoutSymbol]

        def lastInsert = sql.firstRow("""
SELECT accountid,systemLanguage,appLanguage,countryCode,countryName,
        vcCode,vcSymbol,vcFormatWithSymbol,vcFormatWithoutSymbol
FROM localizations WHERE accountid = $account.id ORDER BY time DESC""")

        final boolean nothingNew
        if (lastInsert == null) {
            nothingNew = false
        } else {
            nothingNew = record.every { k, v ->
                Objects.equals(lastInsert[k], v) ||
                        (lastInsert[k] as String == v as String)
            }
        }

        if (!nothingNew) {
            DataSet localizations = sql.dataSet('localizations')
            record.putAll([loginid : loginid, time: time])
            localizations.add(record)
        }
    }

    def addMachinesRecord(Sql sql, int loginid, Timestamp time, account, request, response) {

        def machines = response.lobby.games*.id.findAll { it != 'unknown' }

        def record = [
                accountid: account.id,
                machine1: machines[0],
                machine2: machines[1],
                machine3: machines[2],
                machines: machines.join(',')
        ]

        GroovyRowResult lastRecord = sql.firstRow("""
            SELECT accountid,machine1,machine2,machine3,machines
            FROM machines
            WHERE accountid = $account.id
            ORDER BY time DESC
            """)

        final boolean nothingNew
        if (lastRecord == null) {
            nothingNew = false
        } else {
            nothingNew = record.every { k,v -> lastRecord[k] as String == v as String }
        }

        if (!nothingNew) {
            record.putAll([loginid: loginid, time: time])
            sql.dataSet('machines').add(record)
        }
    }
}
