package tech.ezgames.slots.auditer.db

import groovy.sql.Sql

/**
 * Created by idan on 10 Aug 2016.
 */
interface SqlSupplier {
    Sql getSql()
}