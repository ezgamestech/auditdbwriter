package tech.ezgames.slots.auditer.db

import groovy.sql.Sql
import org.postgresql.ds.PGPoolingDataSource
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
/**
 * Created by idan on 07 Aug 2016.
 */
@Configuration
class RdsPsqlSupplier implements SqlSupplier {

    @Value ('${RDS_HOSTNAME}')
    String rdsHostname

    @Value ('${RDS_PORT}')
    String rdsPort

    @Value ('${RDS_DB_NAME}')
    String rdsDbName

    @Value ('${RDS_USERNAME}')
    String rdsUsername

    @Value ('${RDS_PASSWORD}')
    String rdsPassword


    private PGPoolingDataSource _dataSource

    synchronized PGPoolingDataSource getDataSource() {
        if (_dataSource == null) {
            _dataSource = new PGPoolingDataSource(
                    dataSourceName: 'audit pg pool',
                    serverName: rdsHostname,
                    portNumber: rdsPort as int,
                    databaseName: rdsDbName,
                    user: rdsUsername,
                    password: rdsPassword
            )
        }
        return _dataSource
    }

    @Override
    public Sql getSql() {
//        String url = "jdbc:$rdsEngine://$rdsHostname:$rdsPort/$rdsDbName".toString()
//        Sql.newInstance(url, rdsUsername, rdsPassword, rdsDriver)
        new Sql(dataSource)
    }


}
